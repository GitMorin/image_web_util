# Installation

## Docker // Docker-compose
The fastest way to deploy is using docker-compose:

```
docker-compose up
```

Then access the app via your standard Docker IP:5000

### Using docker

First, build the docker image:

```
docker build -t my_awesome_app .
```

Then run the app:

```
docker run -it my_awesome_app
```


## Without docker

Install the requirements:

```
pip install -r ./requirements.txt
```

Run the app:

```
python app.py
```