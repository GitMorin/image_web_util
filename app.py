from flask import Flask
from flask import request, redirect, render_template
import os, glob
from werkzeug.utils import secure_filename
from PIL import Image
from resizeimage import resizeimage

app = Flask(__name__)

app.config["IMAGE_UPLOADS"] = "./static/uploads"
app.config["THUMB_DIR"] = "./static/thumb/"
app.config["THUMB_SIZE"] = 500, 500
app.config["IMG_MAX_WIDTH"] = 800
app.config["ALLOWED_IMAGE_EXTENSIONS"] = ["JPEG", "JPG", "PNG", "GIF"]
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024 # Default abut 50mb limit
app.config["MAX_IMAGE_FILESIZE"] = 10 * 1024 * 1024

def allowed_image(filename):

    if not "." in filename:
        return False

    ext = filename.rsplit(".", 1)[1]

    if ext.upper() in app.config["ALLOWED_IMAGE_EXTENSIONS"]:
        return True
    else:
        return False


def allowed_image_filesize(filesize):

    if int(filesize) <= app.config["MAX_IMAGE_FILESIZE"]:
        return True
    else:
        return False

def create_thumb(filename):
    im = Image.open(app.config["IMAGE_UPLOADS"]+'/'+filename)
    im.thumbnail(app.config["THUMB_SIZE"])
    im.save(os.path.join(app.config["THUMB_DIR"]+filename), "JPEG")
    return


def resize_max_width(filename, max_width):
    fd_img = open(app.config["IMAGE_UPLOADS"]+'/'+filename)
    img = Image.open(app.config["IMAGE_UPLOADS"]+'/'+filename)
    #img = resizeimage.resize_width(img, app.config["IMG_MAX_WIDTH"])
    img = resizeimage.resize_width(img, int(max_width))
    new_filename = filename.split('.')[0]+"_max_width."+filename.split('.')[1]
    img.save(os.path.join(app.config["THUMB_DIR"]+new_filename), img.format)
    fd_img.close()
    return


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route("/upload-image", methods=["GET", "POST"])
def upload_image():

    if request.method == "POST":
        if request.files:
            if "filesize" in request.cookies:

                if not allowed_image_filesize(request.cookies["filesize"]):
                    print("Filesize exceeded maximum limit")
                    return redirect(request.url)

                image = request.files["image"]

                if image.filename == "":
                    print("No filename")
                    return redirect(request.url)

                if allowed_image(image.filename):
                    filename = secure_filename(image.filename)
                    image.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))
                    #create_thumb(filename)
                    resize_max_width(filename)
                    print("Image saved")
                    return redirect(request.url)
                else:
                    print("That file extension is not allowed")
                    return redirect(request.url)
    return render_template("upload_image.html", title="File upload", page="Upload image")


@app.route("/upload-multiple", methods=["GET", "POST"])
def upload_images():
    if request.method == 'POST' and 'image-multi' in request.files: 
        max_width = request.form['max_width']

        if image.filename == "":
            print("No filename")
            return redirect(request.url)
        
        for image in request.files.getlist('image-multi'):
            if allowed_image(image.filename):
                filename = secure_filename(image.filename)
                image.save(os.path.join(app.config["IMAGE_UPLOADS"], filename))
                resize_max_width(filename, max_width)

                # Delete large pic when done
                os.remove(os.path.join(app.config["IMAGE_UPLOADS"], filename))
            else:
                print("That file extension is not allowed!")
                continue
        return redirect(request.url)
    return render_template("upload_image.html", title="File upload", page="Upload image")


if __name__ == '__main__':
    app.run(host='0.0.0.0')